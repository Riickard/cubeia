export const breakPoints = {
  130: {
    paceBetween: 5,
    slidesPerView: 1,
    slidesPerGroup: 1,
  },
  510: {
    slidesPerView: 3,
    slidesPerGroup: 2,
  },
  660: {
    slidesPerView: 4,
    slidesPerGroup: 3,
  },
  950: {
    slidesPerView: 5,
    slidesPerGroup: 4,
  },
  1130: {
    slidesPerView: 6,
    slidesPerGroup: 5,
  },
  1270: {
    slidesPerView: 7,
    slidesPerGroup: 6,
  },
};
/* export const breakPoints = {
  0: {
    width: 0,
    slidesPerView: 1,
  },
  400: {
    width: 1200,
    slidesPerView: 6,
  },
};
 */
