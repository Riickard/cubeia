import { useCallback, useEffect, useReducer, useState } from "react";
import { reducer } from "./utils/reducer";
import { Data, GameObject } from "./types/data";
import { Reducer } from "react";
import { ReducerAction, Status } from "./types/types";
import FilterByTag from "./components/FilterByTag/FilterByTag";
import FilterByStudio from "./components/FilterByStudio/FilterByStudio";
import GameCard from "./components/Common/GameCard/GameCard";
import Pagination from "./components/Pagination/Pagination";
import FilterByCyrrency from "./components/FilterByCurrency/FilterByCyrrency";
import Layout from "./components/Layout/Layout";
import axios from "axios";
import { CUBEIALOGO, URL } from "./utils/URL";

const initialStatus: Status = {
  isLoading: false,
  isError: null,
};
const initialState = {
  data: {},
  filtered: {},
  currentTag: null,
};

type ReducerType = Reducer<any, ReducerAction>;

function App() {
  const [status, setStatus] = useState<Status>(initialStatus);
  const [state, dispatch] = useReducer<ReducerType>(reducer, initialState);
  const { data, filtered, currentTag } = state;

  // current filter by studio
  const [currentFilter, setCurrentFilter] = useState<string>("");

  const getData = useCallback((data: Data | null): void => {
    dispatch({ type: "GET_DATA", payload: data });
  }, []);
  const getStudioOption = useCallback(
    (option: number | null, studioName: string): void => {
      setCurrentFilter(studioName);
      dispatch({ type: "GAMES_BY_STUDIO", payload: option });
    },
    []
  );

  const getTagOption = useCallback((option: number | null): void => {
    dispatch({ type: "GAMES_STUDIOS_BY_TAG", payload: option });
  }, []);

  const getCurrencyOption = useCallback((option: string | null): void => {
    dispatch({ type: "GAMES_STUDIOS_BY_CURRENCY", payload: option });
  }, []);

  // Reset current studio being displayed text
  useEffect(() => {
    setCurrentFilter("");
  }, [currentTag]);

  // Track and manage pagination
  const [pages, setPages] = useState({
    currentPage: 1,
    lastPage: 0,
    idxPosition: 0,
  });

  /* Get current lenght of games being displayed */
  useEffect(() => {
    if (!filtered) return;
    setPages({
      lastPage: Math.ceil(filtered.games?.length / 50),
      currentPage: 1,
      idxPosition: 0,
    });
  }, [filtered]);

  const paginate = useCallback((option: boolean): void => {
    option
      ? setPages((prev) => ({
          ...prev,
          idxPosition: prev.idxPosition + 50,
          currentPage: prev.currentPage + 1,
        }))
      : setPages((prev) => ({
          ...prev,
          idxPosition: prev.idxPosition - 50,
          currentPage: prev.currentPage - 1,
        }));
  }, []);

  useEffect(() => {
    setStatus((prev) => ({ ...prev, isLoading: true }));
    axios
      .get(URL)
      .then((response) => {
        setStatus((prev) => ({ ...prev, isLoading: false }));
        getData(response.data);
      })
      .catch((error) => {
        console.error(error);
        setStatus({ isError: true, isLoading: false });
      });
  }, []);

  if (status.isLoading || status.isError) {
    return (
      <aside>
        {status.isLoading && <h2>Loading</h2>}
        {status.isError && <h2>Error</h2>}
      </aside>
    );
  }

/*   const text =  currentFilter ? `Games By ${currentFilter}` : "All Games";
 */
  let text5 = currentTag === null ?  `Games By ${currentFilter}` : "All Games";


  return (
    <Layout>
      <nav>
        <img style={{ width: "150px" }} src={CUBEIALOGO} alt="company logo" />
        <FilterByCyrrency setOption={getCurrencyOption} />
      </nav>
      <main>
        <section className="filter-section">
          <FilterByTag tags={data && data.tags} setOption={getTagOption} />
          <FilterByStudio
            studios={data && filtered.studios}
            setOption={getStudioOption}
          />
        </section>
        <section className="navigate-data-section">
          <h3>{text5}</h3>
          <Pagination pages={pages} paginate={paginate} />
        </section>
        <section className="display-data-section">
          {data &&
            filtered.games
              ?.slice(pages.idxPosition, pages.idxPosition + 50)
              .map((game: GameObject) => {
                return <GameCard game={game} />;
              })}
        </section>
      </main>
    </Layout>
  );
}

export default App;
