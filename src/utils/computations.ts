import { Data, GameObject, StudioObject } from "../types/data";

type Filter = (games: GameObject) => void;
type Callback = (data: any) => void;

/* ------------------------------------------------------------------------------ */
/* ---------------------------FILTER STUDIO BY TAG------------------------------- */
/* ------------------------------------------------------------------------------ */

export function filterStudiosByTag(
  data: Data | undefined,
  filterFn: Filter,
  callback: Callback
) {
  if (data === undefined) return;
  const filtredGames: GameObject[] = data?.games.filter(filterFn);
  let studioArr: any[] = [];
  if (data) {
    for (let i = 0; i < filtredGames.length; i++) {
      const { studioId } = filtredGames[i];
      for (let x = 0; x < data.studios.length; x++) {
        const target = data.studios[x];
        if (studioId == target.id) {
          if (!studioArr.includes(target)) {
            studioArr.push(target);
          }
        }
      }
    }
  }
  return callback(studioArr);
}
/* ------------------------------------------------------------------------------ */
/* ---------------------------FILTER ALL BY CURRENCY----------------------------- */
/* ------------------------------------------------------------------------------ */
export function filterAllByCurrency(
  data: Data | undefined,
  option: string,
  callback: Callback
) {
  if (data === undefined) return;
  const filteredStudios: StudioObject[] = data.studios.filter((studio) => {
    if (studio.blockedCurrencies === undefined) return studio;
    if (studio.blockedCurrencies.includes(option) !== true) {
      return studio;
    }
  });
  let filteredGames: GameObject[] = [];
  for (let i = 0; i < data.games.length; i++) {
    const game = data.games[i];
    const target = filteredStudios.some((c) => c.id === game.studioId);
    if (target) filteredGames.push(game);
  }

  const datan = { studios: filteredStudios, games: filteredGames };
  return callback(datan);
}
/* ------------------------------------------------------------------------------ */
/* ------------------------FILTER GAMES WITH TAG BY STUDIO----------------------- */
/* ------------------------------------------------------------------------------ */
export function filterGamesWithTagByStudio(
  data: Data | undefined,
  option: number,
  currentTag: number | null,
  callback: Callback
) {
  if (data === undefined) return;
  const gamesWithCurrentTag = data.games.filter((game) => {
    if (game.gameTags.length) {
      const target = game.gameTags.some((c) => c == currentTag);
      if (target) {
        return game;
      }
    }
  });

  const gamesWithTagAndStudio = gamesWithCurrentTag.filter(
    (game: GameObject) => {
      return game.studioId === option;
    }
  );

  return callback(gamesWithTagAndStudio);
}
