import { Data, GameObject, StudioObject } from "../types/data";
import { ReducerAction, ReducerState } from "../types/types";
import {
  filterAllByCurrency,
  filterGamesWithTagByStudio,
  filterStudiosByTag,
} from "./computations";

export const reducer = (state: ReducerState, action: ReducerAction) => {
  switch (action.type) {
    /* ------------------------------------------------------------------------------ */
    /* ---------------------------------GET_DATA------------------------------------- */
    /* ------------------------------------------------------------------------------ */

    case "GET_DATA":
      let filteredByCurrency: Partial<GameObject[]> = [];
      filterAllByCurrency(action.payload, "EUR", (data) => {
        filteredByCurrency = data;
      });

      return {
        data: action.payload,
        currentTag: null,
        filtered: { ...action.payload, ...filteredByCurrency },
      };

    /* ------------------------------------------------------------------------------ */
    /* ------------------------------GAMES_BY_STUDIO--------------------------------- */
    /* ------------------------------------------------------------------------------ */

    case "GAMES_BY_STUDIO":
      if (action.payload !== null) {
        state.filtered = {
          ...state.filtered,
          games: [...state.data.games],
        };
      }

      const gamesByStudios: GameObject[] = state?.data?.games?.filter(
        (game: GameObject) => {
          if (action.payload == null) return game;
          if (game.studioId == action.payload) return game;
        }
      );
      let filterByTagAndStudio: [] = [];
      filterGamesWithTagByStudio(
        state.data,
        action.payload,
        state.currentTag,
        (data) => {
          filterByTagAndStudio = data;
        }
      );

      return {
        ...state,
        filtered: {
          ...state.filtered,
          games:
            state.currentTag !== null
              ? filterByTagAndStudio
              : [...gamesByStudios],
        },
      };

    /* -------------------------------------------------------------------------------- */
    /* ----------------------------GAMES_STUDIOS_BY_TAG-------------------------------- */
    /* -------------------------------------------------------------------------------- */

    case "GAMES_STUDIOS_BY_TAG":
      if (action.payload === null) {
        state.filtered = state.data;
        return { ...state, currentTag: null, filtered: state.data };
      }

      const filterByTagFn = (game: GameObject) => {
        if (game.gameTags.length) {
          const target = game.gameTags.some((c) => c == action.payload);
          if (target) {
            return game;
          }
        }
      };

      const gamesFilteredByTag = state.data.games?.filter(filterByTagFn);

      let filteredData: any = [];
      filterStudiosByTag(state.data, filterByTagFn, (datan) => {
        filteredData = datan;
      });

      return {
        ...state,
        currentTag: action.payload,
        filtered: {
          ...state.filtered,
          games: [...gamesFilteredByTag],
          studios: [...filteredData],
        },
      };

    /* ------------------------------------------------------------------------------------- */
    /* ----------------------------GAMES_STUDIOS_BY_CURRENCY-------------------------------- */
    /* ------------------------------------------------------------------------------------- */

    case "GAMES_STUDIOS_BY_CURRENCY":
      let filteredD: any = [];
      filterAllByCurrency(state.data, action.payload, (data) => {
        filteredD = data;
      });

      return {
        ...state,
        filtered: { ...state.filtered, ...filteredD },
      };
    default:
      break;
  }
};
