import React, { useState } from "react";
import "./FilterByCurrency.css";
import {
  BsCurrencyBitcoin,
  BsCurrencyEuro,
  BsCurrencyDollar,
} from "react-icons/bs";

type FilterByCurrencyProps = {
  setOption: (currency: string) => void;
};

const FilterByCyrrency: React.FC<FilterByCurrencyProps> = ({ setOption }) => {
  const [currencyOption, setcurrencyOption] = useState("EUR");

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setcurrencyOption(e.target.value);
    setOption(e.target.value);
  };

  return (
    <div className="fbc-container">
      <div className="fbc-box">
        <p className="body">Currency</p>
        <select value={currencyOption} onChange={handleChange}>
          <option value="EUR">EUR</option>
          <option value="USD">USD</option>
          <option value="mBTC">mBTC</option>
        </select>
        {currencyOption === "mBTC" && (
          <BsCurrencyBitcoin size={20} color="#ffffff" />
        )}
        {currencyOption === "EUR" && (
          <BsCurrencyEuro size={20} color="#ffffff" />
        )}
        {currencyOption === "USD" && (
          <BsCurrencyDollar size={20} color="#ffffff" />
        )}
      </div>
    </div>
  );
};

export default FilterByCyrrency;
