import React, { useEffect, useState } from "react";
import ChevronButton from "../Common/ChevronButton/ChevronButton";
import "./FilterByStudio.css";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperType from "swiper/types/swiper-class";
import { Pagination } from "swiper";

import "swiper/css";
import "swiper/css/pagination";
import { StudioObject } from "../../types/data";
import StudioButton from "../Common/StudioButton/StudioButton";
import { breakPoints } from "../../theme/breakpoints";

type FilterByStudioProps = {
  studios?: StudioObject[] | null;
  setOption: (option: number | null, studioName: string) => void;
};

const FilterByStudio: React.FC<FilterByStudioProps> = ({
  studios,
  setOption,
}) => {
  const [swiper, setSwiper] = useState<SwiperType | null>(null);

  return (
    <div className="fbst-container">
      <div className="fbst-swipe-heading-box">
        <h2 className="fbst-heading">Popular Proivders</h2>
        <div className="fbst-swipe-btn-box ">
          <ChevronButton
            isMirrored={true}
            onClick={() => {
              swiper?.slidePrev();
            }}
          />
          <ChevronButton
            isMirrored={false}
            onClick={() => {
              swiper?.slideNext();
            }}
          />
        </div>
      </div>
      <div className="fbst-studio-swiper">
        <Swiper
          modules={[Pagination]}
          spaceBetween={23}
          onInit={(swiper) => setSwiper(swiper)}
          breakpoints={breakPoints}
        >
          {studios ? (
            studios.map((studio: StudioObject, idx: number) => {
              return (
                <SwiperSlide key={idx}>
                  <StudioButton
                    onClick={() => {
                      setOption(studio.id, studio.name);
                    }}
                    studio={studio}
                  />
                </SwiperSlide>
              );
            })
          ) : (
            <h3>No Providers Available</h3>
          )}
        </Swiper>
      </div>
    </div>
  );
};

export default FilterByStudio;
