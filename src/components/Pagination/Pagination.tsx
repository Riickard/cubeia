import React from "react";
import ChevronButton from "../Common/ChevronButton/ChevronButton";
import "./Pagination.css";

type Pagination = {
  currentPage: number;
  lastPage: number;
  idxPosition: number;
};

type PaginationProps = {
  pages?: Pagination;
  paginate: (option: boolean) => void;
};

const Pagination: React.FC<PaginationProps> = ({ pages, paginate }) => {
  return (
    <div className="pgn-container">
      <ChevronButton
        disabled={pages?.currentPage == 1 ? true : false}
        onClick={() => {
          paginate(false);
        }}
        isMirrored={true}
      />
      <h5>{pages?.currentPage}</h5>
      <h5>/</h5>
      <h5>{pages?.lastPage}</h5>
      <ChevronButton
        disabled={pages?.currentPage == pages?.lastPage ? true : false}
        onClick={() => {
          paginate(true);
        }}
        isMirrored={false}
      />
    </div>
  );
};

export default Pagination;
