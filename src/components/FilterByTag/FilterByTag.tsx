import React, { ReactNode, useEffect, useState } from "react";
import { TagsObject } from "../../types/data";
import TagButton from "../Common/TagButton/TagButton";
import "./FilterByTag.css";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperType from "swiper/types/swiper-class";
import { Pagination } from "swiper";

import "swiper/css";
import "swiper/css/pagination";
import ChevronButton from "../Common/ChevronButton/ChevronButton";
import { breakPoints } from "../../theme/breakpoints";

type FilterByTagProps = {
  tags?: TagsObject[];
  setOption: (option: number | null) => void;
};

const FilterByTag: React.FC<FilterByTagProps> = ({ tags, setOption }) => {
  const [swiper, setSwiper] = useState<SwiperType | null>(null);
  const [selectedTag, setSelectedTag] = useState<number | null>(null);

  return (
    <div className="fbt-container">
      <div className="fbt-swipe-heading-box">
        <h2>Game type</h2>
        <div className="fbt-swipe-btn-box">
          <ChevronButton
            isMirrored={true}
            onClick={() => {
              swiper?.slidePrev();
            }}
          />
          <ChevronButton
            isMirrored={false}
            onClick={() => {
              swiper?.slideNext();
            }}
          />
        </div>
      </div>
      <div className="fbt-tag-swiper">
        <Swiper
          modules={[Pagination]}
          spaceBetween={23}
          onInit={(swiper) => setSwiper(swiper)}
          breakpoints={breakPoints}
        >
          <SwiperSlide>
            <button
              className="fbt-tag-all-btn btn"
              onClick={() => {
                setOption(null);
                setSelectedTag(null);
              }}
            >
              All
            </button>
          </SwiperSlide>
          {tags &&
            tags.map((tag: TagsObject, idx: number) => {
              return (
                <SwiperSlide key={idx}>
                  <TagButton
                    onClick={() => {
                      setOption(tag.id);
                      setSelectedTag(tag.id);
                    }}
                    tag={tag}
                    selectedTag={selectedTag}
                  />
                </SwiperSlide>
              );
            })}
        </Swiper>
      </div>
    </div>
  );
};

export default FilterByTag;
