import React, { HTMLAttributes } from "react";
import { GameObject } from "../../../types/data";
import "./GameCard.css";

interface Props extends HTMLAttributes<HTMLDivElement> {
  game?: GameObject;
}

const GameCard: React.FC<Props> = ({ game, ...props }) => {
  return (
    <div {...props} className="game-card">
      <div className="overlay">
        <img src={game?.imageUrl} alt={game?.name} />
      </div>
      <h5>{game?.name}</h5>
    </div>
  );
};

export default React.memo(GameCard);
