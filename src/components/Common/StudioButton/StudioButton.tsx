import React, { HTMLAttributes } from "react";
import { StudioObject } from "../../../types/data";
import "./StudioButton.css";

interface Props extends HTMLAttributes<HTMLButtonElement> {
  studio: StudioObject;
}

const StudioButton: React.FC<Props> = ({ studio, ...props }) => {
  return (
    <button {...props} className="studio-btn btn">
      <img src={studio.imageUrl} alt={studio.icon} />
    </button>
  );
};

export default React.memo(StudioButton);
