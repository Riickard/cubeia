import React, { HTMLAttributes } from "react";
import "./ChevronButton.css";
import { BsChevronLeft, BsChevronRight } from "react-icons/bs";

interface Props extends HTMLAttributes<HTMLButtonElement> {
  isMirrored: boolean;
  disabled?: boolean;
}
const ChevronButton: React.FC<Props> = ({ isMirrored, ...props }) => {
  return (
    <button className="chevron-btn-container btn" {...props}>
      {isMirrored ? (
        <BsChevronLeft size={17} color="#ffffffdf" />
      ) : (
        <BsChevronRight size={17} color="#ffffffdf" />
      )}
    </button>
  );
};

export default React.memo(ChevronButton);
