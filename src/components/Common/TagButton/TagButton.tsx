import React, { HTMLAttributes } from "react";
import { TagsObject } from "../../../types/data";
import "./TagButton.css";

interface Props extends HTMLAttributes<HTMLButtonElement> {
  tag: TagsObject;
  selectedTag: number | null;
}

const TagButton: React.FC<Props> = ({ tag, selectedTag, ...props }) => {
  return (
    <button
      {...props}
      className={`tag-btn btn ${selectedTag == tag.id ? "tag-active" : ""}`}
    >
      {tag.name}
    </button>
  );
};

export default React.memo(TagButton);
