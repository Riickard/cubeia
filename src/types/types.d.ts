export type Status = {
  isLoading: boolean;
  isError: Error | null | boolean;
};
export type ReducerAction = {
  type: string;
  payload: number | null | Data;
};

export type ReducerState = {
  data: Data;
  filtered: Data;
  currentTag: number | null;
};
