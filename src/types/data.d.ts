export type GameObject = {
  id: number;
  name: string;
  mobile: boolean;
  desktop: boolean;
  playForFun: boolean;
  externalId: string;
  mobileExtrernalId: string;
  display: string;
  imageUrl: string;
  gameType: string;
  aspectRatio: string;
  integration: string;
  studioId: number;
  translations: any[];
  releaseDate: number;
  gameTags: number[];
  backgroundImageUrl: string;
  description: string;
};

export type StudioObject = {
  id: number;
  name: string;
  externalId: string;
  integration: string;
  enabled: boolean;
  imageUrl: string;
  blockedCountries: string;
  allowedContries: string;
  blockedCurrencies: string;
  lobbyOrder: number;
  icon: string;
  popular: boolean;
  providerBlockedContries: string;
};

export type CurrenciesObject = {
  integration: string;
  currencies: string[];
  studioId: number;
};
export type TagsObject = {
  display: boolean;
  id: number;
  name: string;
  nameId: string;
  translations: any;
};

export type Data = {
  games: GameObject[];
  studios: StudioObject[];
  currencies: CurrenciesObject[];
  tags: TagsObject[];
};
