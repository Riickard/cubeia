# Game lobby

![alt text](./lobbyimg.png)

## Introduction
This application is made by the guidelines provided.

### Packages used
- Swiper
- Axios

### Features
1. Filter all data by currencies.
2. Filter games by studio option.
3. Filter games and studios by tag option.
4. Filter Reseting when changing tag option for good user experience.
5. Slide / Swiper functionality in studios and tags options
6. Pagination.
7. Filter navigation.


### Performance and Memoization
Componentns and functions effected by react lifecycel is Memoize to improve the speed of the application.
The algorithms used in this app could be improved futher, taking time complexity in consideration.
Overall the speed is good for the user experience. 


## Components
- App
- Layout
- FilterByTag
- FilterByStudio
- FilterByCurrency
- Pagination
- GameCard
- StudioButton
- TagButton
- ChevronButton



### Script
To run application in development
```bash
npm run dev
```

